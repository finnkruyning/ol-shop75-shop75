$(function () {
    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header

    if($('#body_design')) {
        var backLink = $('.crumb-mobile');

        $('#crumbs').append(backLink);
    }

});
