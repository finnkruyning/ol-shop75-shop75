$(function() {


    // This is "probably" IE9 compatible but will need some fallbacks for IE8
    // - (event listeners, forEach loop)

    // wait for the entire page to finish loading
    window.addEventListener('load', function() {

        // setTimeout to simulate the delay from a real page load
        setTimeout(lazyLoad, 1);

    });


    var wrap = (element, wrapper) => {
        element.parentElement.insertBefore(wrapper, element);
        wrapper.appendChild(element);
    };

    var addClassName = (element, className) => {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += ' ' + className;
    };

    function lazyLoad() {
        var lazyImages = document.querySelectorAll('.js-lazy-load');

        // loop over each lazy load image
        lazyImages.forEach(function(lazyImage) {
            var imageUrl = lazyImage.getAttribute('src');

            var imageWrapper = document.createElement("div");
            imageWrapper.className = 'lazy-load-wrapper';
            imageWrapper.style.backgroundImage = 'url('+imageUrl+')';
            var lazyLoadlarge = document.createElement("img");
            lazyLoadlarge.className = 'lazy-load-large';

            wrap(lazyImage, imageWrapper);

            imageWrapper.appendChild(lazyLoadlarge);

            // listen for load event when the new photo is finished loading
            lazyLoadlarge.addEventListener('load', function() {
                // add a class to remove the blur filter to smoothly transition the image change
                setTimeout(function() {
                    imageWrapper.className = imageWrapper.className + ' is-loaded';

                }, 100);
            });

            lazyLoadlarge.src = imageUrl.replace('lowres', '1x');

        });

    }




});
